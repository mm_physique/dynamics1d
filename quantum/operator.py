import numpy as np
from numpy.linalg import matrix_power
from scipy.linalg import expm
import copy
import matplotlib.pyplot as plt

import dynamics1D.quantum.grid
import dynamics1D.quantum.wavefunction

########################################################################
# This script provides, operator classes.
########################################################################

class Operator:
	# Abstract class for an x acting operator
	def __init__(self, grid,hermitian=False):
		self.grid=grid
		self.N=grid.N
		self.hermitian=hermitian
		self.M=[] # Matrix of the operator in X basis !
		self.eigenval=np.zeros(self.N,dtype=np.complex_) 
		self.eigenvec=[] # Eigenstates (from wave function class) id est can be used in both representation
				
	def diagonalize(self):
		# Diagonalize the matrix representation of the operator and 
		# save the eigenvectors as wavefunctions
		if self.M==[]:
			self.fillM()
			
		eigenvec=np.zeros((self.N,self.N),dtype=np.complex_)
		if self.hermitian:
			self.eigenval,eigenvec=np.linalg.eigh(self.M)
		else:
			self.eigenval,eigenvec=np.linalg.eig(self.M)
			
		for i in range(0,self.N):
			wf=dynamics1D.quantum.wavefunction.WaveFunction(self.grid)
			wf.x=eigenvec[:,i]
			wf.normalize("x")
			self.eigenvec.insert(i,wf)
			
	def fillM(self):
		# Fill the matrix representation. To be used then % is overload 
		# and M not explicitively provided.
		self.M=np.zeros((self.N,self.N),dtype=np.complex_)
		for i in range(0,self.N):
			wf=dynamics1D.quantum.wavefunction.WaveFunction(self.grid)
			wf.setState("diracx",i0=i)
			self.M[:,i]=(self%wf).x	
						
	def __mod__(self,other):
		# Operator acting on a wavefunction
		# M%wf <-> M|wf>
		wf=dynamics1D.quantum.wavefunction.WaveFunction(self.grid)
		wf.x=np.matmul(self.M,other.x)
		wf.x2p()
		return wf
		
	def __rmod__(self,other):
		# Operator acting on a wavefunction
		# wf%M <-> <wf|M
		wf=self.__mod__(other)
		wf.x=np.conjugate(wf.x)
		wf.x2p()
		return wf		
			
	def __add__(self,other): 
		# Adding two operators
		operator=Operator(self.grid,hermitian=(self.hermitian and other.hermitian))
		operator.M=self.M+other.M
		return operator
		
	def __sub__(self,other): 
		operator=Operator(self.grid,hermitian=(self.hermitian and other.hermitian))
		operator.M=self.M-other.M
		return operator
			
class Hamiltonian(Operator):
	# Operateur hamiltonien, indépendant du temps.
	# Peut-être construit par : potentiel V(x) ou matrice H
	def __init__(self,grid,beta=0.0,**kwargs):
		Operator.__init__(self,grid)
		self.hermitian=True
		
		if "pot" in kwargs:
			self.potential=kwargs['pot']
			self.Hp=(grid.p-beta*self.grid.h)**2/2
			self.Hx=self.potential.Vx(self.grid.x,0)
			self.withpot=True
			
		if "M" in kwargs:
			self.withpot=False
			self.M=kwargs['M']
			
		if "spectrumfile" in kwargs:
			self.withpot=False
			data=np.load(kwargs['spectrumfile'])
			self.quasienergies=data['quasienergies'][:,0]
			data.close()
			kwargs['Vn']=np.fft.rfft(np.roll(self.quasienergies,int(0.5*(np.size(self.quasienergies))+1)))/self.N
			### ///!!\\\ corrigé le 27/01 il y avait un bug dans la définition des Vn...

	
		if "Vn"	in kwargs:
			self.withpot=False
			self.Vn=kwargs['Vn']
			self.M=np.zeros((self.N,self.N),dtype=np.complex_)
			self.M[0]=np.concatenate((self.Vn,np.conjugate(np.flip(np.delete(self.Vn,0),axis=0))))
			for i in range(1,self.N):
				self.M[i]=np.roll(self.M[0],i)
				
				
		if "spectrumfilenn" in kwargs:
			self.withpot=False
			data=np.load(kwargs['spectrumfilenn'])
			self.quasienergies=data['quasienergies'][:,0]
			data.close()

			e0=0.5*(self.quasienergies[0]+self.quasienergies[int(0.5*(np.size(self.quasienergies)-1))])
			j1=(self.quasienergies[0]-self.quasienergies[int(0.5*(np.size(self.quasienergies)-1))])/2.0
			self.M=np.zeros((self.N,self.N),dtype=np.complex_)
			self.M[0,0]=e0
			self.M[0,1]=j1
			self.M[0,-1]=j1
			for i in range(1,self.N):
				self.M[i]=np.roll(self.M[0],i)
			
		# WIP 04/06 : double puit TB ?
		if "spectrumfile2ilot" in kwargs:
			self.withpot=False
			data=np.load(kwargs['spectrumfile2ilot'])
			self.qEsym=data['qEsym']
			self.qEasym=data['qEasym']
			data.close()
			self.Vnsym=np.fft.rfft(np.roll(self.qEsym,int(0.5*(np.size(self.qEsym)-1))))/self.N			
			self.Vnasym=np.fft.rfft(np.roll(self.qEasym,int(0.5*(np.size(self.qEasym)-1))))/self.N
			self.M=np.zeros((self.N,self.N),dtype=np.complex_)
			
			
			Msym=np.repeat(np.concatenate((self.Vnsym,np.conjugate(np.flip(np.delete(self.Vnsym,0),axis=0)))),2)
			Masym=np.repeat(np.concatenate((self.Vnasym,np.conjugate(np.flip(np.delete(self.Vnasym,0),axis=0)))),2)
			sign=np.ones(self.N)
			sign[1::2]=-1
			self.M[0]=(Msym+sign*Masym)
			
			
			sign=np.ones(self.N)
			sign[::2]=-1
			self.M[1]=(Msym+sign*Masym)
			
			
			
			for i in range(2,self.N):
				if (i%2)==0:
					self.M[i]=np.roll(self.M[0],i)
				else:
					self.M[i]=np.roll(self.M[1],i-1)
			
			
			
			
	def __mod__(self,other):
		# Hamiltonian acting on a wavefunction
		# H%wf <-> H|wf>
		if self.withpot: # via split-step method
			wfx=dynamics1D.quantum.wavefunction.WaveFunction(self.grid)
			wfx.x=other.x*self.Hx
			wfp=dynamics1D.quantum.wavefunction.WaveFunction(self.grid)
			wfp.p=other.p*self.Hp
			wfp.p2x()
			return wfx+wfp
		else: # via matrix multiplication
			return Operator.__mod__(self,other)
			
	
class TimePropagator(Operator):
	# Propagateur temporel pour hamiltonien indépendant du temps.
	# U(dt)|psi(t)>=exp(-i*H*dt/h)|psi(t)>=|psi(t+dt)>

	def __init__(self,grid,ham,dt=1,beta=0.0,**kwargs):
		Operator.__init__(self,grid)
		self.hermitian=False
		self.dt=dt 
		self.hamiltonian=ham
		if self.hamiltonian.M==[]:
			self.hamiltonian.fillM()
			
		self.M=expm(-1j*self.hamiltonian.M*self.dt/self.grid.h)
		
		# ~ print(self.M[0,0],self.M[0,1],self.M[0,-1])
		# ~ print(self.M[-1,0],self.M[-1,-2],self.M[-1,-1])
		# ~ self.M=0.5*(self.M+np.transpose(self.M))
			
class FloquetTimePropagator(Operator):
	# Propagateur temporel pour hamiltonien dépendant du temps, de façon périodique.
	# U(T0)|psi(t)>=|psi(T0+dt)>
	# Il peut être utilisé en donnant: 
	# - un potentiel V(x), il propage alors en split-step method
	# - un hamiltonien, il propage par produit matriciel 

	def __init__(self,grid,potential,beta=0.0,alpha=0.0,T0=1,idtmax=1000,randomphase=False,kicked=False,symstep=True,kinphases=[]):
		Operator.__init__(self,grid)
		self.quasienergy=np.zeros(grid.N) # quasi energies
		
		self.kicked=kicked
		self.symstep=symstep
		
		self.T0=T0
		self.idtmax=idtmax
		
		if self.kicked:
			self.idtmax=1
		
		
		self.dt=self.T0/self.idtmax
		self.potential=potential
		self.beta=beta
		self.alpha=alpha
		self.randomphase=randomphase
		
		if self.randomphase:
			if kinphases==[]:
				self.Up=np.exp(-1j*(np.random.rand(self.N)*2*np.pi))
			else:
				self.Up=np.exp(1j*kinphases)
			#self.Up=np.ones(self.N)
		else:
			self.Up=np.exp(-1j*((self.grid.p-self.beta*grid.h)**2/4)*self.dt/self.grid.h)
			
		x,t=np.meshgrid(grid.x,np.arange(self.idtmax)*self.dt)	
		self.Ux=np.exp(-1j*(self.potential.Vx(x-self.alpha*self.grid.dx,t))*self.dt/self.grid.h)

		
	def __mod__(self,other):
		# Floquet propagator acting on a wavefunction
		# U%wf <-> U|wf>
		wf=copy.deepcopy(other)
		if self.randomphase:
			for idt in range(0,self.idtmax):
				wf.p=wf.p*self.Up 
				wf.p2x() 
				wf.x=wf.x*self.Ux[idt]
				wf.x2p() 
		elif self.symstep:
			for idt in range(0,self.idtmax):
				wf.p=wf.p*self.Up 
				wf.p2x() 
				wf.x=wf.x*self.Ux[idt]
				wf.x2p() 
				wf.p=wf.p*self.Up 
				wf.p2x() 
		else:
			for idt in range(0,self.idtmax):
				wf.x=wf.x*self.Ux[idt]
				wf.x2p()
				wf.p=wf.p*self.Up
				wf.p=wf.p*self.Up  
				wf.p2x()  
		return wf
		
	def diagonalize(self):
		# Diagonalize, then compute quasi-energies
		Operator.diagonalize(self)
		for i in range(0,self.N):
			self.quasienergy[i]=-np.angle(self.eigenval[i])*(self.grid.h/self.T0)	
			
	def propagates(self,wf,t1,t2):
		i1=int(t1/self.dt)
		i2=int(t2/self.dt)
		for idt in range(i1,i2):
			wf.p=wf.p*self.Up 
			wf.p2x() 
			wf.x=wf.x*self.Ux[idt]
			wf.x2p() 
			wf.p=wf.p*self.Up 
			wf.x2p() 
		
	def orderEigenstatesWithOverlapOn(self,wf):
		overlaps=np.zeros(self.N,dtype=complex)
		for i in range(0,self.N):
			overlaps[i]=self.eigenvec[i]%wf
			
		ind=np.flipud(np.argsort(np.abs(overlaps)**2))
		
		self.eigenvec=[self.eigenvec[i] for i in ind]
		self.eigenval=self.eigenval[ind]
		self.quasienergy=self.quasienergy[ind]
		
		return overlaps[ind]	
	
		
	def diffQuasienergy(self,qe1,qe2):
		# This returns the difference on a circle
		de=np.pi*(self.grid.h/self.T0)
		diff=qe1-qe2
		if diff>de:
			return diff-2*de
		elif diff<-de:
			return diff+2*de
		else:
			return diff
				
	def getTunnelingPeriod(self):
		return 2*np.pi*self.grid.h/(self.T0*(abs(self.diffQuasienergy(self.quasienergy[0],self.Quasienergy[1]))))
		
	def getFormFactor(self,it):
		# Returns form factor for given time/n
		n=int(it)
		return abs(np.sum(self.eigenval**n))**2/self.N	
		
	# ~ def getSpacingDistribution(self,bins=50):
		# ~ # WIP prob
		
		# ~ symX=np.zeros(self.N,dtype=bool)
		# ~ s=np.zeros(self.N)
		
		# ~ for i in range(0,self.N):
			# ~ symX[i]=self.eigenvec[i].isSym("x")

		
		# ~ for j in range(2):
			# ~ if j==0:
				# ~ indSym=np.where(symX==True)
				# ~ i0=0
			# ~ if j==1:
				# ~ indSym=np.where(symX==False)
				# ~ i0=len(np.where(symX==True)[0])
				
				
			# ~ qE=self.quasienergy[indSym]
			# ~ ind=np.argsort(qE)

			
			
			# ~ for i in range(len(ind)):
				# ~ qe1=qE[ind[i]]
				# ~ if i!= (len(ind)-1):
					# ~ qe2=qE[ind[i+1]]
				# ~ else:
					# ~ qe2=qE[ind[0]]
				
				# ~ mls=2*np.pi*self.grid.h/self.T0/len(ind) #mean level spacing
				# ~ s[i0+i]=np.abs(self.diffQuasienergy(qe1,qe2))/mls

		# ~ return np.histogram(s, range=(0,5), bins=bins,density=True)

			
	def getSpacingDistribution(self,bins=50):
		# WIP prob
		
		s=np.zeros(self.N)
		qE=self.quasienergy
		ind=np.argsort(qE)
		
		for i in range(len(ind)):
			qe1=qE[ind[i]]
			if i!= (len(ind)-1):
				qe2=qE[ind[i+1]]
			else:
				qe2=qE[ind[0]]
				
			mls=2*np.pi*self.grid.h/self.T0/len(qE) #mean level spacing
			s[i]=np.abs(self.diffQuasienergy(qe1,qe2))/mls		

		return np.histogram(s, range=(0,5), bins=bins,density=True)

class TDTimePropagator(Operator):
	# Propagateur temporel pour hamiltonien dépendant du temps, générique (cas défavorable)
	# U(T0,t)|psi(t)>=|psi(t+T0)>
	# Il peut être utilisé en donnant: 
	# - un potentiel V(x), il propage alors en split-step method
	# - un hamiltonien, il propage par produit matriciel 

	def __init__(self,grid,potential,dt=2*np.pi/1000):
		Operator.__init__(self,grid)

		self.dt=dt
		self.potential=potential
		self.Up=np.exp(-1j*(self.grid.p**2/4)*self.dt/self.grid.h)

		
	def propagate(self,wf,t,ndt=1):
		# Floquet propagator acting on a wavefunction
		# U%wf <-> U|wf>
		wf=copy.deepcopy(wf)
		for i in range(ndt):
			wf.p=wf.p*self.Up 
			wf.p2x() 
			wf.x=wf.x*np.exp(-1j*(self.potential.Vx(self.grid.x,t))*self.dt/self.grid.h)
			wf.x2p() 
			wf.p=wf.p*self.Up 
			wf.p2x() 
			t=t+self.dt
		return wf,t
