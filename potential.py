import numpy as np

class Potential:
	# The class potential is desgined to be herited only
	def __init__(self):	
		pass
	def Vx(self,x,t=0):
		pass
	def dVdx(self,x,t=0):
		pass	

class ModulatedPendulum(Potential):
	def __init__(self,e,gamma,mw=np.cos,Fb=0,phi=0,nuC=0,heff=0.0):
		Potential.__init__(self)
		
		self.e=e
		self.gamma=gamma
		self.mw=mw
		self.Fb=Fb # Bloch
		self.phi=phi
		
		nuL=8111.25
		self.omega=heff*nuC/(2*nuL)
		
	def Vx(self,x,t=0):
		return -self.gamma*(1+self.e*self.mw(t+self.phi))*np.cos(x)+self.Fb*x/(2*np.pi)+0.5*self.omega**2*x**2
	
	def dVdx(self,x,t=0):
		return self.gamma*(1+self.e*self.mw(t+self.phi))*np.sin(x)
		
		
class ModulatedPendulumDisorder(Potential):
	#WIIIIP
	def __init__(self,e,gamma,elocal):
		Potential.__init__(self)
		
		self.e=e
		self.gamma=gamma
		self.elocal=elocal
		
	def Vx(self,x,t=0):
		return -self.gamma*(1+self.e*np.cos(t))*np.cos(x)+self.elocal
	
		
		
		
class DoubleWell(Potential):
	def __init__(self,e,gamma):
		Potential.__init__(self)
		self.gamma=gamma
		self.e=e
		
	def Vx(self,x,t=0):
		return -self.gamma*(np.cos(x)-self.e*np.cos(2*x))
	
	def dVdx(self,x,t=0):
		return self.gamma*(np.sin(x)-self.e*2*np.sin(2*x))
		
		
class DoubleWellKicked(Potential):
	def __init__(self,gamma,x0=np.pi/2):
		Potential.__init__(self)
		self.gamma=gamma
		self.x0=x0

	def Vx(self,x,t=0):
		return self.gamma*(x**2-self.x0**2)**2/self.x0**4

	def dVdx(self,x,t=0):
		return self.gamma*2*(x**2-self.x0**2)*2*x/self.x0**4
		
class SawTooth(Potential):
	# Potential en dent de scie
	def __init__(self,gamma,dx=2*np.pi):
		Potential.__init__(self)
		self.gamma=gamma
		self.dx=dx
		
	def Vx(self,x,t=0):
		return -2*np.pi*self.gamma*np.mod((x+np.pi)/self.dx,1.0)
			
class Rectangle(Potential):
	# Potential en créneau
	def __init__(self,gamma,x1=-np.pi/2,x2=np.pi/2):
		Potential.__init__(self)
		self.x1=x1
		self.x2=x2
		self.gamma=gamma
		
	def Vx(self,x,t=0):
		return -2*np.pi*self.gamma*np.array(x>self.x1)*np.array(x<self.x2)
		
		
class KickedRotor(Potential):
	def __init__(self,K):
		Potential.__init__(self)
		self.K=K
		
	def Vx(self,x,t=0):
		return -self.K*np.cos(x) #V(x) = +K*cos(x) centré en 0...
	
	def dVdx(self,x,t=0):
		return self.K*np.sin(x)
		
class HarmonicOscillator(Potential):
	def __init__(self,omega):
		Potential.__init__(self)
		self.omega=omega
		
	def Vx(self,x,t=0):
		return 0.5*self.omega**2*x**2
		
		
class LoadingQM():
    def __init__(self,beta0,gamma,heff,alphac=0.01):

        
        self.Fc=(np.pi/2)*(gamma/heff)**2
        self.alphac=alphac
        self.F=-self.alphac*self.Fc
        self.beta0=beta0 #quasimoment voulu
        self.Tswap=np.abs(beta0*heff/self.F) #temps d'accélération
        
        self.v=self.F*self.Tswap
        
    def phi(self,t):
        return (t<self.Tswap)*self.F*t**2/2+(t>=self.Tswap)*(self.F*self.Tswap**2/2+self.v*(t-self.Tswap))
    
    def mw(self,t):
        return (t<self.Tswap)*0.0 + (t>=self.Tswap)*np.cos(t-self.Tswap)
    
    
class ModulatedPendulumReal(Potential):
    def __init__(self,e,gamma,protocole,nuC=0.0,heff=0.0): #heff sert si confinement
        Potential.__init__(self)
        self.e=e
        self.gamma=gamma
        
        nuL=8111.25
        
        self.mw=protocole.mw
        self.phi=protocole.phi
        self.omega=heff*nuC/(2*nuL)

    def Vx(self,x,t=0):
        return -self.gamma*(1+self.e*self.mw(t))*np.cos(x+self.phi(t))+0.5*self.omega**2*x**2
		
		
		

